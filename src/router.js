import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import About from './views/About.vue'
import PurchaseOrders from './views/PurchaseOrders.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '/purchaseOrders',
      name: 'purchaseOrders',
      component: PurchaseOrders
    }
  ]
})
