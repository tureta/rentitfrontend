import Vue from 'vue'
import App from './App.vue'
import router from './router'
import animated from 'animate.css'
import Buefy from 'buefy'
import 'buefy/lib/buefy.css'
import moment from "moment"

Vue.use(Buefy)
Vue.use(require('vue-moment'));

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')

Vue.filter('formatDate', function(value) {
  if (value) {
    return moment(String(value)).format('MM/DD/YYYY')
  }
})
